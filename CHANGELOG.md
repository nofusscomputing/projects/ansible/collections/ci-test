## Unreleased

### Fix

- use correct submodule branch
- add missing submodule job

## 0.2.1-a1 (2024-02-22)

### Fix

- ci branch name

## 0.2.0 (2024-02-17)

### Fix

- master merge release
- don't jam pipeline success for MR to master

### Refactor

- make change to check master branch merge and auto release

## 0.1.0-a8 (2024-02-16)

### Feat

- new job name to create release

### Fix

- git tag to happen after change commit
- checkout tag correctly

## 0.1.0-a7 (2024-02-16)

### Feat

- dont run var job for bot

## 0.1.0-a6 (2024-02-16)

### Fix

- build. only adjust to git tag head on git tag.

## 0.1.0-a5 (2024-02-16)

### Fix

- build. only adjust to git tag head on git tag.
- build on dev and tag build ensure on git tag commit

## 0.1.0-a4 (2024-02-16)

### Fix

- test if feat bump to 0.2.0-a1

## 0.1.0-a3 (2024-02-16)

### Feat

- test if feat bump to 0.2.0-a1
- **ci**: update with fixes

## 0.1.0-a2 (2024-02-16)
